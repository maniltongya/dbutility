#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include "include/SQLAPI.h"

using namespace std;

struct Config {
    string server;
    string username;
    string password;
};

string getParameters(string s) {
    string delimiter = "=";
    size_t pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        s.erase(0, pos + delimiter.length());
    }
    return s;
}

void loadConfig(Config& config) {
    ifstream fin("config.txt");
    string line;
    while (getline(fin, line)) {
        istringstream sin(line.substr(line.find("=") + 1));
        if (line.find("server") != -1)
            sin >> config.server;
        else if (line.find("username") != -1)
            sin >> config.username;
        else if (line.find("password") != -1)
            sin >> config.password;
    }
}

string createQuery(string table, string columnNames) {
    string query = "SELECT " + columnNames + " FROM " + table;
    return query;
}

vector<vector <string>> fetchDataFromTable(string tableName, string columnNames) {
    SAConnection con;
    SACommand cmd(&con);
    vector<vector <string>> data;
    try {
        Config config;
        loadConfig(config);
        con.Connect(config.server.c_str(), config.username.c_str(), config.password.c_str(), SA_SQLServer_Client);
        SAString query = createQuery(tableName, columnNames).c_str();
        cmd.setCommandText(_TSA(query));
        cmd.Execute();
        int nResulSets = 0;
        int countOfColumn = cmd.FieldCount();
        while (cmd.isResultSet())
        {
            printf("Processing result set #%d\n", ++nResulSets);
            while (cmd.FetchNext())
            {
                vector<string> row;
                for (int i = 1; i <= countOfColumn; i++) {
                    SAString s = cmd[i].asString();
                    row.push_back(s.GetMultiByteChars());
                }
                data.push_back(row);
            }
        }
        con.Disconnect();
        return data;
    }
    catch (SAException& x) {
        con.Rollback();
        printf("%s\n", x.ErrText().GetMultiByteChars());
    }
}