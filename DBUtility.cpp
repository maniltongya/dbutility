#include "../DBUtility.h"

using namespace std;

int main()
{
    vector<vector<string>> data = fetchDataFromTable("productdata_master", "ProductID, SKU");
    //vector<vector<string>> data = fetchDataFromTable("rating", "entity_id, rating_code");
    for (int i = 0; i < data.size(); i++) {
        for (int j = 0; j < data[i].size(); j++) {
            cout << data[i][j] << " - ";
        }
        cout << endl;
    }
    return 0;
}
